import { defineMessages } from 'react-intl';

/**
 * Internationalized messages for use on general components.
 */
export default defineMessages({
  language: {
    id: 'localeSelect.language',
    defaultMessage: 'Language',
  },
  selectOrganisation: {
    id: 'formInputs.organisation.select',
    defaultMessage: 'Select organization',
  },
  country: {
    id: 'formInputs.country.select',
    defaultMessage: 'Country',
  },
  definition: {
    id: 'foooter.definition',
    defaultMessage:
      'Mapathon Keralam - An initiative of Kerala State IT Mission with support from Rebuild Kerala',
  },
  credits: {
    id: 'footer.credits',
    defaultMessage: 'Mapathon Keralam - An initiative of Kerala State IT Mission with support from Rebuild Kerala',
  },
  learn: {
    id: 'footer.learn',
    defaultMessage: 'Learn more about OpenStreetMap.',
  },
  privacyPolicy: {
    id: 'footer.privacyPolicy',
    defaultMessage: 'Privacy Policy',
  },
  mappingLevelALL: {
    id: 'mapping.level.all',
    defaultMessage: 'All levels',
  },
  mappingLevelADVANCED: {
    id: 'mapping.level.advanced',
    defaultMessage: 'Advanced mapper',
  },
  mappingLevelINTERMEDIATE: {
    id: 'mapping.level.intermediate',
    defaultMessage: 'Intermediate mapper',
  },
  mappingLevelBEGINNER: {
    id: 'mapping.level.beginner',
    defaultMessage: 'Beginner mapper',
  },
});
